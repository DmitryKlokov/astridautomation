package astridAutomation.step_definitions;

import astridAutomation.page_objects.ActionBarPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.junit.Before;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;


public class ActionBarSteps {

    private ActionBarPage actionBarPage = new ActionBarPage();



    @And("^click back$")
    public void clickBack() throws Throwable {
        actionBarPage.backMenu();
    }

    @And("^open list (.*)$")
    public void openList(String listName) throws Throwable {
        actionBarPage.openList(listName);
    }

    @When("^open search$")
    public void openSearch() throws Throwable {
        actionBarPage.openMenu();
        actionBarPage.chooseSubmenuSearch();
    }

    @And("^enter in the search field '(.*)'$")
    public void enterInTheSearchField(String searchString) throws Throwable {
        actionBarPage.search(searchString);
    }

    @When("^Open sort options list$")
    public void openSortOptionsList() throws Throwable {
        actionBarPage.openMenu();
        actionBarPage.chooseSubmenuSort();
    }

    @When("^open lists$")
    public void openLists() throws Throwable {
        actionBarPage.openLists();
    }

    @And("^click new list$")
    public void clickNewList() throws Throwable {
        actionBarPage.openNewLists();
    }


    @And("^lists contains '(.*)'$")
    public void listsContains(String listName) throws Throwable {
        if(! actionBarPage.listsContains(listName)) throw new Exception();
    }

    @And("^lists does not contains '(.*)'$")
    public void listsDoesNotContains(String listName) throws Throwable {
        if( actionBarPage.listsContains(listName)) throw new Exception();
    }

    @Then("^check list '(.*)' in lists$")
    public void checkListInLists(String listName) throws Throwable {
        //!!!!!!!
    }

    @And("^open menu$")
    public void openMenu() throws Throwable {
        actionBarPage.openMenu();
    }

}
