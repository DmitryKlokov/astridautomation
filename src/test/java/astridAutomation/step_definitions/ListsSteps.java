package astridAutomation.step_definitions;

import astridAutomation.page_objects.NewListPage;
import astridAutomation.page_objects.TaskListPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

/**
 * Created by dmitr_000 on 11/9/2016.
 */
public class ListsSteps {

    NewListPage newListPage = new NewListPage();
    TaskListPage taskListPage = new TaskListPage();

    @And("^enter list name = '(.*)'$")
    public void enterListName(String listName) throws Throwable {
        newListPage.enterListName(listName);
    }

    @And("^enter contact or email = '(.*)'$")
    public void enterContactOrEmail(String contactOrEmail) throws Throwable {
        newListPage.enterContactOrEmail(contactOrEmail);
    }

    @And("^enter description = '(.*)'$")
    public void enterDescription(String description) throws Throwable {
        newListPage.enterDescription(description);
    }

    @And("^check silence notifications = '(.*)'$")
    public void checkSilenceNotifications(String notification) throws Throwable {
        newListPage.checkSilenceNotifications(notification);
    }

    @And("^create new list = '(.*)'$")
    public void createNewList(String newList) throws Throwable {
        //!!!!!
    }

    @Then("^don't share appears$")
    public void donTShareAppears() throws Throwable {
        taskListPage.DoNotShareAppears();
    }

    @And("^press don't share$")
    public void pressDonTShare() throws Throwable {
        taskListPage.DoNotSharePress();
    }

    @And("^check date equal '(.*)'$")
    public void checkDateEqual(String date) throws Throwable {
        taskListPage.checkDateEqualToday(date);
    }

    @And("^press delete button$")
    public void pressDeleteButton() throws Throwable {
        newListPage.pressDelete();
    }

    @And("^press delete OK$")
    public void pressDeleteOK() throws Throwable {
        newListPage.pressDeleteOk();
    }
}
