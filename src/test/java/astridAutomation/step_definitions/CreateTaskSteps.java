package astridAutomation.step_definitions;

import astridAutomation.models.Task;
import astridAutomation.page_objects.TaskListPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.util.List;

public class CreateTaskSteps {

    private TaskListPage taskPage = new TaskListPage();

    @And("^quick add task '(.*)'$")
    public void quickAddTask(String task) {
        taskPage.quickAddTask(task);
    }

    @And("^press quick add task button$")
    public void pressQuickAddTaskButton() throws Throwable {
        taskPage.pressQuickAddTaskButton();
    }

    @And("^quick add tasks$")
    public void quickAddTasks(List<String> tasks) throws Throwable {
        for(String task : tasks) {
            this.quickAddTask(task);
            this.pressQuickAddTaskButton();
        }
    }

    @Given("^add tasks$")
    public void addTasks (List<Task> tasks) throws Throwable {
        for(Task task : tasks) this.quickAddTask(task.GetTitle());
    }

    @Then("^add assigned to quick add '(.*)'$")
    public void addAssignedToQuickAdd(String assigned) throws Throwable {
        taskPage.addAssigned(assigned);
    }

    @And("^close all messages$")
    public void closeAllMessages() throws Throwable {
        taskPage.clickOnTheQuickAdd();
    }
}
