package astridAutomation.step_definitions;

import astridAutomation.page_objects.ActionBarPage;
import astridAutomation.page_objects.SettingsPage;
import astridAutomation.page_objects.SignPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SignSteps {
    private SignPage signPage = new SignPage();
    private ActionBarPage actionBarPage = new ActionBarPage();
    private SettingsPage settingsPage = new SettingsPage();

    @Given("^the first launch of the application$")
    public void theFirstLaunchOfTheApplication() throws Throwable {

    }

    @Given("^sign page is opened$")
    public void signPageIsOpened() throws Throwable {
        actionBarPage.openMenu();
        actionBarPage.chooseSubmenuSettings();
        settingsPage.opemSignUpForAFreeCccount();
    }

    @And("^click late on the sign in page$")
    public void clickLateOnTheSignInPage() throws Throwable {
        signPage.clickOnLater();
    }

    @And("^click no thanks$")
    public void clickNoThanks() throws Throwable {
        signPage.clickOnNoThanks();
    }

    @And("^click button X$")
    public void clickButtonX() throws Throwable {
        signPage.clickOnButtonX();
    }


    @When("^sign up is opened$")
    public void signUpIsOpened() throws Throwable {
        signPage.openSignUp();
    }

    @And("^enter firstname, lastname and email = (.*), (.*), (.*)$")
    public void enterFirstnameLastnameAndEmail(String firstname, String lastname, String email) throws Throwable {
        signPage.signUp(firstname, lastname, email);
    }

    @Then("^please wait appears$")
    public void pleaseWaitAppears() throws Throwable {
        signPage.pleaseWait();
    }
}
