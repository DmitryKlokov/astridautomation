package astridAutomation.step_definitions;

import astridAutomation.page_objects.EditTaskPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

import java.text.SimpleDateFormat;


public class EditTaskSteps {

    private EditTaskPage editTaskPage = new EditTaskPage();

    @When("^open edit '(.*)' task form$")
    public void openEditTask(String title) {
        editTaskPage.openTaskInTheTaskList(title);
    }

    @And("^edit title '(.*)'$")
    public void editTitle(String title) throws Throwable {
        editTaskPage.editTaskTitle(title);
    }

    @And("^edit complete '(.*)'$")
    public void editComplete(String complete) throws Throwable {
        //Boolean.parseBoolean(complete);
    }

    @And("^edit assigned '(.*)'$")
    public void editAssigned(String assigned) throws Throwable {
        editTaskPage.editAssigned(assigned);
    }

    @And("^edit priority '(.*)'$")
    public void editPriority(String priority) throws Throwable {
        editTaskPage.editPriority(Integer.parseInt(priority));
    }

    @And("^edit category '(.*)'$")
    public void editCategory(String category) throws Throwable {
        editTaskPage.editCategory(category);
    }

    @And("^edit description '(.*)'$")
    public void editDescription(String description) throws Throwable {
        editTaskPage.editDescription(description);
    }

    @And("^edit comment '(.*)'$")
    public void editComment(String comment) throws Throwable {
        editTaskPage.editComment(comment);
    }

    @And("^check priority '(.*)'$")
    public void checkPriority(String priority) throws Throwable {
        if(editTaskPage.checkPriority(priority)) throw new PendingException();
    }



    @And("^check description '(.*)'$")
    public void checkDescription(String description) throws Throwable {
        if(! editTaskPage.checkDescription(description)) throw new PendingException();
    }

    @And("^check comment '(.*)'$")
    public void checkComment(String comment) throws Throwable {
        if(! editTaskPage.checkComment(comment)) throw new PendingException();
    }

    @And("^press delete$")
    public void pressDelete() throws Throwable {
        editTaskPage.pressDelete();
        editTaskPage.pressOkDelete();
    }

    @And("^press OK$")
    public void pressOK() throws Throwable {
        editTaskPage.pressOkDelete();
    }

    @And("^check category equal '(.*)'$")
    public void checkCategoryEqualNewCategory(String category) throws Throwable {
        if(! editTaskPage.checkCategory(category)) throw new PendingException();
    }
}
