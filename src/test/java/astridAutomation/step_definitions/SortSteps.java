package astridAutomation.step_definitions;

import astridAutomation.page_objects.SortPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;

public class SortSteps {

    private SortPage sortPage = new SortPage();

    @And("^choose option - '(.*)'$")
    public void chooseOptionByTitle(String option) throws Throwable {
        sortPage.sortOption(option);
    }

    @And("^submit just once$")
    public void submitJustOnce() throws Throwable {
        sortPage.submitJustOnce();
    }

    @And("^choose deleted task option '(.*)'$")
    public void chooseHiddenTaskOption(String option) throws Throwable {
        sortPage.chooseDeletedTaskOption(option);
    }
}
