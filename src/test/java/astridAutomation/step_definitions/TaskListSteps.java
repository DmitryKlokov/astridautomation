package astridAutomation.step_definitions;

import astridAutomation.page_objects.SignPage;
import astridAutomation.page_objects.TaskListPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class TaskListSteps {

    private TaskListPage taskListPage = new TaskListPage();

    @Given("^open task list$")
    public void openTaskList(){
        if(! taskListPage.isOpened()){
            SignPage signPage =  new SignPage();
            if( signPage.isOpened() )
            {
                signPage.clickOnLater();
                signPage.clickOnNoThanks();
                signPage.clickOnButtonX();
            }
        }
    }

    @Then("^find task by title = '(.*)'$")
    public void findTaskByTitle(String title) throws Throwable {
        if(! taskListPage.checkTaskInTheTaskList(title))throw new Exception();
    }

    @Then("^not find task by title = '(.*)'$")
    public void notFindTaskByTitle(String title) throws Throwable {
        if(taskListPage.checkTaskInTheTaskList(title))throw new Exception();
    }

    @Then("^find task by title, category and description  = (.*), (.*), (.*)$")
    public void findTaskByTitleCategoryAndDescription(String title, String category, String description) throws Throwable {
        taskListPage.checkTaskInTheTaskList(title,category, description);
    }

    @Then("^tasks that contains appear '(.*)'$")
    public void tasksAppearThatContainsTask(String searchText) throws Throwable {
        if(! taskListPage.countTaskContains(searchText)) throw new Exception();
    }

    @Then("^task '(.*)' before '(.*)'$")
    public void taskTaskSortBeforeTaskSort(String firstTask, String secondTask) throws Throwable {
        if(! taskListPage.checkSort(firstTask, secondTask)) throw new Exception();
    }

    @And("^open edit list$")
    public void openEditList() throws Throwable {
        taskListPage.openEditList();
    }


}
