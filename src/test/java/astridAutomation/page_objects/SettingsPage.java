package astridAutomation.page_objects;

import astridAutomation.framework.AndroidObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;


public class SettingsPage  extends AndroidObject {

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Sign up for a free account']")
    private MobileElement signUpForAFreeCccount;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='Cancel']")
    private MobileElement voiceInput;

    public void opemSignUpForAFreeCccount(){
        waitUntilElementDisplayed(voiceInput);
        voiceInput.click();
        waitUntilElementDisplayed(signUpForAFreeCccount);
        signUpForAFreeCccount.click();
    }
}
