package astridAutomation.page_objects;

import astridAutomation.framework.AndroidObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class SortPage  extends AndroidObject {

    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='By title']")
    private MobileElement sortOptionByTitle;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='Just once']")
    private MobileElement sortSubmitJustOnce;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Show deleted tasks']")
    private MobileElement showDeletedTasks;

    public void sortOption(String option){
        waitUntilElementDisplayed(sortOptionByTitle);
        sortOptionByTitle.click();
    }
    public void submitJustOnce(){
        sortSubmitJustOnce.click();
    }
    public void chooseDeletedTaskOption(String option){
        waitUntilElementDisplayed(showDeletedTasks);
        showDeletedTasks.click();
    }
}
