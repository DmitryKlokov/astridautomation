package astridAutomation.page_objects;

import astridAutomation.framework.AndroidObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

import java.util.List;

public class EditTaskPage  extends AndroidObject {

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[@index=0]/android.widget.LinearLayout/android.widget.EditText")
    private MobileElement title;


    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=2]/android.widget.LinearLayout[@index=0]/android.widget.ToggleButton[@index=3]")
    private MobileElement priority0;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=2]/android.widget.LinearLayout[@index=0]/android.widget.ToggleButton[@index=5]")
    private MobileElement priority1;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=2]/android.widget.LinearLayout[@index=0]/android.widget.ToggleButton[@index=7]")
    private MobileElement priority2;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=2]/android.widget.LinearLayout[@index=0]/android.widget.ToggleButton[@index=9]")
    private MobileElement priority3;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=2]/android.widget.LinearLayout[@index=0]/android.widget.ToggleButton[@checked='true']")
    private MobileElement priorityChecked;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=3]/android.widget.LinearLayout[@index=0]/android.widget.TextView")
    private MobileElement categoryText;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=3]/android.widget.LinearLayout[@index=0]/android.widget.ImageView")
    private MobileElement categoryButton;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='New list']")
    private MobileElement categoryNewList;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='OK']")
    private MobileElement categoryButtonOk;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=0]/android.widget.ImageButton[@index=1]")
    private MobileElement categoryRemove;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=4]/android.widget.LinearLayout[@index=0]/android.widget.TextView")
    private MobileElement description;

    @AndroidFindBy(xpath = "//android.widget.EditText")
    private MobileElement descriptionText;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='OK']")
    private MobileElement descriptionButton;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[@index=1]/android.widget.EditText")
    private MobileElement commentAddText;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[@index=1]/android.widget.ImageButton[@index=2]")
    private MobileElement commentAddButton;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=1]/android.widget.LinearLayout[@index=0]/android.widget.LinearLayout[@index=0]/android.widget.ImageView")
    private MobileElement assignedOpenButton;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[@index=0]/android.widget.ImageButton")
    private MobileElement assignedClearButton;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[@index=0]/android.widget.EditText")
    private MobileElement assignedEditText;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[@index=0]/android.widget.Button")
    private MobileElement assignedAddButton;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.widget.LinearLayout/android.widget.ImageView")
    private MobileElement delete;

    @AndroidFindBy(xpath = "//*[@text='OK']")
    private MobileElement deleteOk;

    public void openTaskInTheTaskList(String title){
        By xpathTask = By.xpath("//android.widget.TextView[@text='"+title+"']");
        waitUntilElementDisplayed(xpathTask);
        getAndroidDriver().findElement(xpathTask).click();
    }

    public void editTaskTitle(String newTitle){
        waitUntilElementDisplayed(title);
        title.clear();
        waitUntilElementDisplayed(title);
        title.sendKeys(newTitle);
    }


    public void editAssigned(String assigned){
        assignedOpenButton.click(); //open assigned form
        assignedClearButton.click();//clear
        assignedEditText.sendKeys(assigned);
        assignedAddButton.click();
    }

    public void editPriority(int newPriority){
        waitUntilElementDisplayed(priority0);
        switch(newPriority)
        {
            case 0: priority0.click(); break;
            case 1: priority1.click(); break;
            case 2: priority2.click(); break;
            case 3: priority3.click(); break;
        }
    }

    public boolean checkPriority(String priority){
        waitUntilElementDisplayed(priorityChecked);
        return priorityChecked.getText().equals(priority);
    }
    public void editCategory(String newCategory){
        waitUntilElementDisplayed(categoryButton);
        categoryButton.click();
        waitUntilElementDisplayed(categoryNewList);
        categoryNewList.sendKeys(newCategory);
        waitUntilElementDisplayed(categoryButtonOk);
        categoryButtonOk.click();
    }
    public boolean checkCategory(String category){
        waitUntilElementDisplayed(categoryText);
        return categoryText.getText().equals(category);
    }

    public void editDescription(String newDescription){
        waitUntilElementDisplayed(description);
        description.click();
        waitUntilElementDisplayed(descriptionText);
        descriptionText.clear();
        descriptionText.sendKeys(newDescription);
        descriptionButton.click();
    }

    public boolean checkDescription(String desc){
        waitUntilElementDisplayed(description);
        return description.getText().equals(desc);
    }

    public void editComment(String newComment){
        waitUntilElementDisplayed(commentAddText);
        commentAddText.sendKeys(newComment);
        waitUntilElementDisplayed(commentAddButton);
        commentAddButton.click();
    }

    public boolean checkComment(String comment){
        List elements = getAndroidDriver().findElements(By.name("You commented: "+comment));
        return elements.size()>0;
    }
    public void pressDelete(){
        waitUntilElementDisplayed(delete);
        delete.click();
    }
    public void pressOkDelete(){
        waitUntilElementDisplayed(deleteOk);
        deleteOk.click();
    }
}
