package astridAutomation.page_objects;

import astridAutomation.framework.AndroidObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.List;

public class TaskListPage extends AndroidObject {

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=1]/android.widget.EditText[@index=0]")
    private MobileElement quickAddText;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=1]/android.widget.ImageButton[@index=1]")
    private MobileElement quickAddButton;

    @AndroidFindBy(xpath = "//*[@text='Assigned to me']")
    private MobileElement assignedButton;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Contact or Email']")
    private MobileElement assignedTextEmpty;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button")
    private MobileElement assignedAddButon;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index=2]/android.widget.LinearLayout[@index=0]/android.widget.Button[@index=0]")
    private MobileElement doNotShare;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[android.widget.HorizontalScrollView]/android.widget.ImageView")
    private MobileElement editList;

    public boolean isOpened(){
        try {
            quickAddText.getText();
            return true;
        }
        catch (NoSuchElementException e){
            return false;
        }
    }

    public void quickAddTask(String task){
        waitUntilElementDisplayed(quickAddText);
        quickAddText.sendKeys(task);
    }

    public void pressQuickAddTaskButton(){
        waitUntilElementDisplayed(quickAddButton);
        quickAddButton.click();
    }

    public void checkCompleteBox(String title){
        waitUntilElementDisplayed(By.xpath("//android.widget.RelativeLayout" +
                "[android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[@text='"+title+"']]" +
                "/android.widget.ImageView"));
        getAndroidDriver().findElement(By.xpath("//android.widget.RelativeLayout" +
                "[android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[@text='"+title+"']]" +
                "/android.widget.ImageView")).click();
    }

    private void checkTaskCategory(String title, String category){
        waitUntilElementDisplayed(By.xpath("//android.widget.LinearLayout[android.widget.TextView" +
                "[@text='"+title+"']]" +
                "/android.widget.LinearLayout/android.widget.TextView[@text='"+category+"']"));
    }
    private void checkTaskDescription(String title, String description){
        waitUntilElementDisplayed(By.xpath("//android.widget.LinearLayout" +
                "[android.widget.LinearLayout/android.widget.TextView[@text='" + title + "']]" +
                "/android.widget.LinearLayout[@index=1]/android.widget.ImageView"));
        getAndroidDriver().findElement(By.xpath("//android.widget.LinearLayout" +
                "[android.widget.LinearLayout/android.widget.TextView[@text='" + title + "']]" +
                "/android.widget.LinearLayout[@index=1]/android.widget.ImageView")).click();
        waitUntilElementDisplayed(By.xpath("//android.widget.TextView[@text='"+description+"']"));
        getAndroidDriver().findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
    }
    public void checkTaskInTheTaskList(String title, String category, String description)
    {
        waitUntilElementDisplayed(By.xpath("//android.widget.TextView[@text='"+title+"']"));
        if(category!=null) checkTaskCategory(title,category);
        if(description!=null) checkTaskDescription(title,description);
    }
    public boolean checkTaskInTheTaskList(String title)
    {
        waitUntilElementDisplayed(By.xpath("//android.widget.TextView"));
        List<MobileElement> elements = getAndroidDriver().findElements(By.xpath("//android.widget.TextView[@text='"+title+"']"));
        if(elements.size()==0) return false;
        return true;
    }
    public boolean countTaskContains(String searchField){
        List<MobileElement> tasks = getAndroidDriver().findElements(By.xpath("//android.widget.TextView"));
        for( MobileElement element : tasks )
        {
            //if( element.getText().contains(searchField))
            return true;
        }
        return false;
    }

    public boolean checkSort(String firstTask, String secondTask){
        WebElement first = getAndroidDriver().findElement(By.xpath("//android.widget.LinearLayout" +
                "[android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[@text='"+firstTask+"']]"));
        WebElement second = getAndroidDriver().findElement(By.xpath("//android.widget.LinearLayout" +
                "[android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[@text='"+secondTask+"']]"));
        return first.getLocation().y<second.getLocation().y;
    }
    public void DoNotShareAppears(){
        waitUntilElementDisplayed(doNotShare);
    }
    public void DoNotSharePress(){
        doNotShare.click();
    }
    public void addAssigned(String assigned){
        assignedButton.click();
        waitUntilElementDisplayed(assignedTextEmpty);
        assignedTextEmpty.sendKeys(assigned);
        assignedAddButon.click();
    }
    public void clickOnTheQuickAdd(){
        waitUntilElementDisplayed(quickAddText);
        quickAddText.click();
    }
    public void checkDateEqualToday(String date){
        waitUntilElementDisplayed(By.xpath("//android.widget.LinearLayout[@index=0]/android.widget.LinearLayout[@index=1]/android.widget.TextView[@text='"+date+"']"));
    }
    public void openEditList(){
        waitUntilElementDisplayed(editList);
        editList.click();
    }
}
