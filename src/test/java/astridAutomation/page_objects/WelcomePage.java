package astridAutomation.page_objects;
import astridAutomation.framework.AndroidObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class WelcomePage extends AndroidObject{

    @AndroidFindBy(xpath = "//*[@text='Next']")
    private MobileElement nextButton;

    public void clickOnNext() {
        waitUntilElementDisplayed(nextButton);
        nextButton.click();
    }
}
