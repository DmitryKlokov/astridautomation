package astridAutomation.page_objects;

import astridAutomation.framework.AndroidObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.NoSuchElementException;

public class SignPage extends AndroidObject {

    @AndroidFindBy(xpath = "//*[@text='Later']")
    private MobileElement later;

    @AndroidFindBy(xpath = "//*[@text='No thanks']")
    private MobileElement noThanks;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[@index='0']/android.widget.ImageView[@index='1']")
    private MobileElement buttonX;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Sign up']")
    private MobileElement signUpOpenButton;


    //sign up

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='First Name']")
    private MobileElement firstNameInput;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Last Name']")
    private MobileElement lastNameInput;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Email']")
    private MobileElement emailInput;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='OK']")
    private MobileElement signUpOkButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Please Wait']")
    private MobileElement pleaseWait;

    public boolean isOpened(){
        try {
            later.getText();
            return true;
        }
        catch (NoSuchElementException e){
            return false;
        }
    }

    public void clickOnLater() {
        waitUntilElementDisplayed(later);
        later.click();
    }
    public void clickOnNoThanks() {
        waitUntilElementDisplayed(noThanks);
        noThanks.click();
    }
    public void clickOnButtonX() {
        waitUntilElementDisplayed(buttonX);
        buttonX.click();
    }
    public void openSignUp(){
        signUpOpenButton.click();
    }
    public void signUp(String firstName, String lastName, String email){
        waitUntilElementDisplayed(firstNameInput);
        firstNameInput.sendKeys(firstName);
        lastNameInput.sendKeys(lastName);
        emailInput.sendKeys(email);
        signUpOkButton.click();
    }
    public void pleaseWait(){
        waitUntilElementDisplayed(pleaseWait);
        getAndroidDriver().navigate().back();
        getAndroidDriver().navigate().back();
        getAndroidDriver().navigate().back();
    }
}
