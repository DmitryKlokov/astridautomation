package astridAutomation.page_objects;


import astridAutomation.framework.AndroidObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class NewListPage extends AndroidObject {

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.EditText")
    private MobileElement listNameText;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Contact or Email']")
    private MobileElement contactOrEmailText;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Type a description here']")
    private MobileElement descriptionText;

    @AndroidFindBy(xpath = "//android.widget.CheckBox")
    private MobileElement notificationText;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='Delete list']")
    private MobileElement deleteButton;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='OK']")
    private MobileElement deleteButtonOk;

    public void enterListName(String listName){
        waitUntilElementDisplayed(listNameText);
        listNameText.clear();
        listNameText.sendKeys(listName);
    }
    public void enterContactOrEmail(String contactOrEmail){
        contactOrEmailText.sendKeys(contactOrEmail);
    }
    public void enterDescription(String description){
        descriptionText.sendKeys(description);
    }
    public void checkSilenceNotifications(String notification){
        boolean flag = Boolean.parseBoolean(notification);
        if(flag) notificationText.click();
    }
    public void pressDelete(){
        waitUntilElementDisplayed(deleteButton);
        deleteButton.click();
    }
    public void pressDeleteOk(){
        waitUntilElementDisplayed(deleteButtonOk);
        deleteButtonOk.click();
    }
}
