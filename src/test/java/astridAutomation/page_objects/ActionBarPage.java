package astridAutomation.page_objects;

import astridAutomation.framework.AndroidObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

import java.util.List;


public class ActionBarPage extends AndroidObject {

    @AndroidFindBy (xpath = "//android.widget.LinearLayout/android.widget.ImageView")
    private MobileElement menuButton;

    @AndroidFindBy (xpath = "//android.widget.TextView[@text='Settings']")
    private MobileElement subMenuSettings;

    @AndroidFindBy (xpath = "//android.widget.TextView[@text='Search']")
    private MobileElement subMenuSearch;

    @AndroidFindBy (xpath = "//android.widget.TextView[@text='Sort']")
    private MobileElement subMenuSort;

    @AndroidFindBy (xpath = "//android.widget.FrameLayout/android.widget.ImageView")
    private MobileElement backMenu;

    @AndroidFindBy (xpath = "//android.widget.FrameLayout[@index=0]/android.view.View/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView")
    private MobileElement lists;

    @AndroidFindBy (xpath = "//android.widget.SearchView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")
    private MobileElement searchField;


    public void backMenu(){
        waitUntilElementDisplayed(backMenu);
        backMenu.click();
    }
    public void openMenu(){
        waitUntilElementDisplayed(menuButton);
        menuButton.click();
    }
    public void chooseSubmenuSettings(){
        waitUntilElementDisplayed(subMenuSettings);
        subMenuSettings.click();
    }
    public void chooseSubmenuSearch(){
        waitUntilElementDisplayed(subMenuSearch);
        subMenuSearch.click();
    }
    public void chooseSubmenuSort(){
        waitUntilElementDisplayed(subMenuSort);
        subMenuSort.click();
    }
    public void openLists(){
        waitUntilElementDisplayed(lists);
        lists.click();
    }
    public void openList(String listName){
        getAndroidDriver().findElement(By.xpath("//android.widget.TextView[@text='"+listName+"']")).click();
    }
    public boolean listsContains(String listName){
        List elements = getAndroidDriver().findElements(By.xpath("//android.widget.TextView[@text='"+listName+"']"));
        if(elements.size()>0) return true;
        return false;
    }
    public void search(String searchString){
        waitUntilElementDisplayed(searchField);
        searchField.sendKeys(searchString);
        getAndroidDriver().pressKeyCode(AndroidKeyCode.ENTER);
    }
    public void openNewLists(){
    }

}
