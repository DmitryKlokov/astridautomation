package astridAutomation;

import astridAutomation.page_objects.WelcomePage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "json:target/REPORT_NAME.json", "pretty",
                "html:target/HTML_REPORT_NAME" },
        features = "src/test/resources/features"
        ,glue={"astridAutomation/step_definitions"}
)

public class testRunner {

    @BeforeClass
    public static void init()
    {
        WelcomePage welcomePage = new WelcomePage();
        for (int i = 0; i < 6; i++) welcomePage.clickOnNext();
    }

    @AfterClass
    public static void teatDown()
    {
        WelcomePage welcomePage = new WelcomePage();
        welcomePage.close();
    }
}
