package astridAutomation.framework;

import astridAutomation.framework.helpers.AndroidHelper;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public abstract class AndroidObject {

    private AndroidDriver androidDriver;
    private WebDriverWait wait;

    protected AndroidDriver getAndroidDriver(){
        return androidDriver;
    }

    protected AndroidObject() {
        this.androidDriver = AndroidHelper.getAndroidDriver();
        this.wait = new WebDriverWait(androidDriver, AndroidHelper.getDriverWaitTime());
        PageFactory.initElements(new AppiumFieldDecorator(this.getAndroidDriver(), 5, TimeUnit.SECONDS), this);
    }

    protected void waitUntilElementDisplayed(By by){
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    protected void waitUntilElementDisplayed(MobileElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void close(){
        AndroidHelper.closeAppiumDriver();
    }
}
