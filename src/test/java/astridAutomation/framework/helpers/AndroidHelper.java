package astridAutomation.framework.helpers;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class AndroidHelper extends EventFiringWebDriver {

    private static final long DRIVER_WAIT_TIME = Long.parseLong(PropertyManager.getProperty("Driver_wait_time"));
    private static AndroidDriver<MobileElement> ANDROID_DRIVER = null;

    static {
        ANDROID_DRIVER = startAppiumDriver();
    }


    private AndroidHelper() {
        super(ANDROID_DRIVER);
    }

    private static AndroidDriver<MobileElement> startAppiumDriver() {
        DesiredCapabilities capabilities = getAppiumDesiredCapabilities();
        try{
            ANDROID_DRIVER = new AndroidDriver<MobileElement>(new URL(PropertyManager.getProperty("appium_url")), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return ANDROID_DRIVER;
    }

    private static DesiredCapabilities getAppiumDesiredCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, PropertyManager.getProperty("device_name"));
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PropertyManager.getProperty("platform_name"));
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, PropertyManager.getProperty("automation_name"));
        capabilities.setCapability("appPackage", PropertyManager.getProperty("package_name"));
        capabilities.setCapability("appActivity", PropertyManager.getProperty("launchable_activity"));
        capabilities.setCapability(MobileCapabilityType.APP, PropertyManager.getProperty("app_path"));
        return capabilities;
    }

    public static long getDriverWaitTime(){
        return DRIVER_WAIT_TIME;
    }
    public static AndroidDriver<MobileElement> getAndroidDriver() {
        return ANDROID_DRIVER;
    }

    public static void closeAppiumDriver(){
        ANDROID_DRIVER.quit();
    }
}