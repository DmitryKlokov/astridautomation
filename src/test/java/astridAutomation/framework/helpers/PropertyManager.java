package astridAutomation.framework.helpers;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

final class PropertyManager {
    private static Properties properties;

    static {
        try {
            load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private PropertyManager() {
    }


    private static void load() throws IOException {
        properties = new Properties();
        String propFileName =   "Atrid.properties";
        try{
            InputStream inputStream = PropertyManager.class.getClassLoader().getResourceAsStream(propFileName);
            properties.load(inputStream);
         }
         finally {

        }
    }

    static String getProperty(final String key) {
        final String propertyValue = properties.getProperty(key);
        return propertyValue != null ? propertyValue : "Requested property does not exist";
    }
}
