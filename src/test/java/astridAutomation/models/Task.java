package astridAutomation.models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Task
{
    private String title;
    private boolean complete;
    private String assigned;
    private Date date;
    private int priority;
    private String category;
    private String description;
    private String comment;

    public Task(String title, String complete , String assigned, String date, String priority, String category, String description, String comment) throws Throwable{
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        this.title = title;
        this.complete = Boolean.parseBoolean(complete);
        this.assigned = assigned;
        this.date =  formatter.parse(date);
        this.priority = Integer.parseInt(priority);
        this.category = category;
        this.description = description;
        this.comment = comment;
    }
    public String GetTitle(){
        return this.title;
    }
    public boolean getComplete(){
        return this.complete;
    }
    public String getAssigned(){
        return this.assigned;
    }
    public Date getDate(){
        return this.date;
    }
    public int getPriority(){
        return this.priority;
    }
    public String getCategory(){
        return this.category;
    }
    public String getDescription(){
        return this.description;
    }
    public String getComment(){
        return this.comment;
    }

    @Override
    public String toString(){
        return "title="+title
                + ", complete=" + complete
                + ", assigned=" + assigned
                + ", date=" + date
                + ", priority=" + priority
                + ", category=" + category
                + ", description=" + description
                + ", comment=" + comment;
    }
}
