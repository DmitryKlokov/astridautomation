Feature: edit

  Background: add tasks
    Given open task list
    And quick add task 'newTask1'
    And press quick add task button

  Scenario: edit title
    When open edit 'newTask1' task form
    And edit title 'newTitle'
    And click back
    And close all messages
    Then find task by title = 'newTitle'

  Scenario: edit priority
    When open edit 'newTask1' task form
    And edit priority '3'
    And click back
    And close all messages
    Then open edit 'newTask1' task form
    And check priority '!'
    And click back

  Scenario: edit category
    When open edit 'newTask1' task form
    And edit category 'newCategory'
    And click back
    And close all messages
    Then open edit 'newTask1' task form
    And check category equal 'newCategory'
    And click back


  Scenario: edit description
    When open edit 'newTask1' task form
    And edit description 'newDescription'
    And click back
    And close all messages
    Then open edit 'newTask1' task form
    And check description 'newDescription'
    And click back

  Scenario: edit comment
    When open edit 'newTask1' task form
    And edit comment 'newDescription'
    And click back
    And close all messages
    Then open edit 'newTask1' task form
    And check comment 'newDescription'
    And click back


