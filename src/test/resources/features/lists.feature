Feature: lists
  Scenario: check date tasks by default in the today list
    Given open task list
    And open lists
    And open list Today
    When quick add task 'newTask00'
    And press quick add task button
    Then check date equal 'today'

  Scenario Outline: create new list
    Given open task list
    And open lists
    When open list New List
    And enter list name = '<listName>'
    And enter contact or email = '<contactOrEmail>'
    And enter description = '<description>'
    And check silence notifications = '<notifications>'
    And click back
    Then open lists
    And lists contains '<listName>'
    And open list Today

    Examples:
      |listName|contactOrEmail|description|notifications|
      |list1   |op@gmail       |abcd       |true         |

   Scenario: delete list
     Given open task list
     And open lists
     When open list list1
     And open edit list
     And press delete button
     And press delete OK
     Then open lists
     And lists does not contains 'newListName'
     When open list New List
     And click back

  Scenario: edit list name
    Given open task list
    And open lists
    And open list New List
    And enter list name = 'listName'
    And click back
    When open edit list
    And enter list name = 'newListName'
    And click back
    Then open lists
    And lists contains 'newListName'
