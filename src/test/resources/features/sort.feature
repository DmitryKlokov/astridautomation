Feature: sort
  Scenario: Sort
    Given open task list
    And quick add tasks
      |taskSort2|
      |taskSort1|
    When Open sort options list
    And choose option - 'By title'
    And submit just once
    Then task 'taskSort1' before 'taskSort2'

  Scenario: show deleted tasks
    Given open task list
    And quick add task 'deletedTask'
    And press quick add task button
    And open edit 'deletedTask' task form
    And press delete
    When Open sort options list
    And choose deleted task option 'Show deleted tasks'
    And submit just once
    Then find task by title = 'deletedTask [deleted]'
