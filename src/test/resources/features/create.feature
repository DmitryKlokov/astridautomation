Feature: createTask

  Scenario: quick create task
    Given open task list
    And quick add task 'Hello world!'
    And press quick add task button
    Then find task by title = 'Hello world!'

  Scenario: quick create task with assigned
    Given open task list
    And quick add task 'Hello world2!'
    And add assigned to quick add 'assigned@mail.ru'
    And press quick add task button
    Then don't share appears
    And press don't share
    And press quick add task button

  Scenario: delete task
    Given open task list
    And quick add task 'taskForDelete'
    And press quick add task button
    When open edit 'taskForDelete' task form
    And press delete
    Then not find task by title = 'taskForDelete'