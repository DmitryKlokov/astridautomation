Feature: sign

  Scenario Outline: sign up
    Given open task list
    And sign page is opened
    When sign up is opened
    And enter firstname, lastname and email = <firstname>, <lastname>, <email>
    Then please wait appears

    Examples:
      |firstname|lastname|email|
      |Vasia    |Pupkin  |vasiapupkin@gmail.com|
